-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Nov-2017 às 09:07
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `neppo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `age` int(3) NOT NULL,
  `cpf_cnpj` varchar(14) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `customers`
--

INSERT INTO `customers` (`id`, `name`, `birthdate`, `age`, `cpf_cnpj`, `address`, `sex`, `created`, `modified`) VALUES
(1, 'Thalles Oliveira', '1996-04-16', 21, '054.459.311-10', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'M', '2017-11-20 19:01:50', '2017-11-20 19:01:50'),
(4, 'Gabriel Humberto', '1996-04-28', 21, '054.054.054-44', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'M', '2017-11-20 19:45:09', '2017-11-20 19:45:09'),
(5, 'Thais Almeida', '1986-04-16', 31, '04405506622', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'F', '2017-11-21 02:08:43', '2017-11-21 02:08:43'),
(6, 'Maria da Silva', '1876-05-12', 46, '055.055.055-10', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'F', '2017-11-21 02:55:42', '2017-11-21 02:55:42'),
(7, 'Lucas Borges', '1985-11-15', 19, '054.459.168-10', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'M', '2017-11-21 03:11:58', '2017-11-21 03:11:58'),
(8, 'Matheus Rodrigues', '2007-12-15', 9, '054.258.169-10', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'M', '2017-11-21 03:13:07', '2017-11-21 03:13:07'),
(9, 'Andressa Viana', '2002-11-10', 8, '155.022.033-10', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'F', '2017-11-21 04:47:37', '2017-11-21 04:55:41'),
(12, 'Amanda Castro', '1996-04-16', 21, '05445931110', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'F', '2017-11-21 05:00:17', '2017-11-21 05:00:17'),
(13, 'Robson Marques', '1976-04-16', 61, '155.022.033-10', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'M', '2017-11-21 05:02:53', '2017-11-21 05:03:18'),
(14, 'Diogo Reis ', '1996-04-16', 21, '155.022.033-10', 'Rua HorÃ¡cio Paulo Siqueira, 52A', 'M', '2017-11-21 05:03:53', '2017-11-21 05:03:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
