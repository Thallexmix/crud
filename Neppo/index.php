<?php require_once 'config.php'; ?>	
<?php require_once DBAPI; ?>		
<?php include(HEADER_TEMPLATE); ?>	
<?php $conn = open_database(); ?>
<?php include('functions.php');
?>	

<h1>Gráficos</h1>
<hr />


<?php if ($conn) : ?>	
<?php
$cont_idade9 = 0;
$cont_idade19 = 0;
$cont_idade29 = 0;
$cont_idade39 = 0;
$cont_idade40 = 0;
$cont_feminino = 0;
$cont_masculino = 0;



$clientes = grafico($conn);
foreach($clientes as $cliente){
	
	
	
	if($cliente['age'] < 10 )
		$cont_idade9++;
	
	elseif($cliente['age']>9 && $cliente['age']<20)
		$cont_idade19++;
	
	elseif($cliente['age'] > 19 && $cliente['age'] < 30)
		$cont_idade29++;
	
	elseif($cliente['age'] > 29 && $cliente['age'] < 40)
		$cont_idade39++;
	
	else
		$cont_idade40++;


}

$clientes = grafico($conn);
foreach($clientes as $cliente){



    if($cliente['sex'] == 'F')
        $cont_feminino++;

    else
        $cont_masculino++;

}

?>

 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Idades", "Quantidade", { role: "style" } ],
        ["0 a 9", <?=$cont_idade9?>, "#FF0000"],
        ["10 a 19", <?=$cont_idade19?>, "gold"],
        ["20 a 29", <?=$cont_idade29?>, "#000080"],
        ["30 a 39", <?=$cont_idade39?>, "#008080"],
		  ["+ de 40", <?=$cont_idade40?>, "#00FFFF"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title:  "Grafico de Idades",
        width: 600,
        height: 400,
        bar: {groupWidth: "70%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>
 
 
	
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js%22%3E"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Sexo', 'Quantidade'],
          ['Feminino',     <?=$cont_feminino?>],
          ['Masculino',      <?=$cont_masculino?>]
        ]);

        var options = {
          title: 'Grafico por Sexo'
			
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  
 
 
<div class="container">
	<div class="row">
		<div class="col-sm-6"> 
<div id="piechart" style="width: 800px; height: 400px;"></div>
</div>
<div class="col-sm-6">
<div id="columnchart_values" style="width: 800px; height: 200px;"></div>
 </div>
<?php else : ?>			
	<div class="alert alert-danger" role="alert">		
	<p><strong>ERRO:</strong> Não foi possível Conectar ao Banco de Dados!</p>			
	</div>
		
<?php endif; ?>
	
<?php include(FOOTER_TEMPLATE); ?>
</body>
</html>
