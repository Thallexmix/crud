<?php 		  
require_once('../functions.php'); 		  
edit();		
?>

<?php include(HEADER_TEMPLATE); ?>

<h2>Atualizar Cliente</h2>

<form action="edit.php?id=<?php echo $customer['id']; ?>" method="post">		  
	<hr />		  
		<div class="row">		    
		<div class="form-group col-md-7">		      
			<label for="name">Nome / Razão Social</label>		      
			<input type="text" class="form-control" name="customer['name']" value="<?php echo $customer['name']; ?>">		    
		</div>
		
		<div class="form-group col-md-3">		      
			<label for="campo2">CNPJ / CPF</label>		      
			<input type="text" class="form-control" name="customer['cpf_cnpj']" value="<?php echo $customer['cpf_cnpj']; ?>">		    
		</div>
		</div>	
		<div class="row">
		<div class="form-group col-md-2">		      
			<label for="campo3">Data de Nascimento</label>		      
			<input type="text" class="form-control" name="customer['birthdate']" value="<?php echo $customer['birthdate']; ?>">		    
		</div>
		<div class="form-group col-md-2">		      
			<label for="campo3">Idade</label>		      
			<input type="text" class="form-control" name="customer['age']" value="<?php echo $customer['age']; ?>">		    
		</div>			  
		</div>		 
		<div class="row">		    
		<div class="form-group col-md-5">		      
			<label for="campo1">Endereço</label>		      
			<input type="text" class="form-control" name="customer['address']" value="<?php echo $customer['address']; ?>">		    
		</div>
		<div class="form-group col-md-2">		      
			<label for="campo3">Sexo</label>		      
			<input type="text" class="form-control" name="customer['sex']" value="<?php echo $customer['sex']; ?>">		    
		</div>		  
		</div>		  
	<div id="actions" class="row">		    
		<div class="col-md-12">		      
			<button type="submit" class="btn btn-primary">Salvar</button>		      
			<a href="index.php" class="btn btn-default">Cancelar</a>		    
		</div>		  
	</div>		
</form>

<?php include(FOOTER_TEMPLATE); ?>