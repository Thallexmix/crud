<?php 		  
	require_once('../functions.php'); 		  
	add();		
?>	
	
<?php include(HEADER_TEMPLATE); ?>		

<h2>Novo Cliente</h2>


<form action="add.php" method="post">

	<hr />		  
		<div class="row">		    
			<div class="form-group col-md-7">		      
				<label for="name">Nome / Razão Social</label>		      
				<input type="text" class="form-control" name="customer['name']">		    
			</div>
		
			<div class="form-group col-md-3">		      
				<label for="campo2">CNPJ / CPF</label>		      
				<input type="text" class="form-control" name="customer['cpf_cnpj']" placeholder="Ex:XXX.XXX.XXX-XX">		    
			</div>
		</div>
		<div class="row">	
			<div class="form-group col-md-2">		      
			 	<label for="campo3">Data de Nascimento</label>		      
			 	<input type="text" class="form-control" name="customer['birthdate']" placeholder="Ex:YYYY-MM-DD">		    
			</div>	
			  
			<div class="form-group col-md-2">		      
				<label for="campo3">Idade</label>		      
				<input type="text" class="form-control" name="customer['age']">		    
			</div>
		</div>		
		<div class="row">    
			<div class="form-group col-md-5">		      
				<label for="campo1">Endereço</label>		      
				<input type="text" class="form-control" name="customer['address']">		    
			</div>
		
			<div class="form-group col-md-2">		      
				<label for="campo3">Sexo</label>		      
				<input type="text" class="form-control" name="customer['sex']" placeholder="Ex:M ou F">		    
			</div>
		</div>
		<div class="row">
			<div id="actions" >		    
				<div class="col-md-12" class="row">		      
					<button type="submit" class="btn btn-primary">Salvar</button>		      
					<a href="index.php" class="btn btn-default">Cancelar</a>		    
				</div>		  
			</div>
		</div>		
		</form>
		
<?php include(FOOTER_TEMPLATE); ?>
			
			
			