<?php 			
	require_once('../functions.php'); 			
	view($_GET['id']);		
?>
		
<?php include(HEADER_TEMPLATE); ?>

<h2>Cliente <?php echo $customer['id']; ?></h2>		
<hr>

<?php if (!empty($_SESSION['message'])) : ?>			
	<div class="alert alert-<?php echo $_SESSION['type']; ?>"><?php echo $_SESSION['message']; ?></div>		
<?php endif; ?>


<dl class="dl-horizontal">			
	<dt>Nome / Razão Social:</dt>			
	<dd><?php echo $customer['name']; ?></dd>		
	
	<dt>CPF / CNPJ:</dt>			
	<dd><?php echo $customer['cpf_cnpj']; ?></dd>		
	
	<dt>Data de Nascimento:</dt>			
	<dd><?php echo $customer['birthdate']; ?></dd>
	
	<dt>Idade:</dt>			
	<dd><?php echo $customer['age']; ?></dd>
	
	<dt>Endereço:</dt>			
	<dd><?php echo $customer['address']; ?></dd>
		
	<dt>Sexo:</dt>			
	<dd><?php echo $customer['sex']; ?></dd>
			
</dl>

<div id="actions" class="row">			
	<div class="col-md-12">			  
		<a href="edit.php?id=<?php echo $customer['id']; ?>" class="btn btn-primary">Editar</a>			  
		<a href="index.php" class="btn btn-default">Voltar</a>			
	</div>		
</div>	
	
<?php include(FOOTER_TEMPLATE); ?>